---
title: Coronavirus student project
---

A PB051 bioinformatics project at the Faculty of Informatics at MU in Brno.

#### Goals (with web pages to create):

1. Collect as many coronavirus sequences as possible (Page 1 - Sequence data)
1. Using HMMER create a profile-HMM of coronavirus sequence and/or its important subsequences (Page 2 - HMM Alignment)
1. Find other related sequences in public sequence databases (Page 3 - Related sequences)
1. Create a labeled phylogenetic tree of the aligned sequences (Page 4 - Phylogenetic trees)
1. Phylogeographic analysis (Page 5 - Phylogeography)
1. Time series analysis (Page 6 - Time series)

### Dependencies

#### R
- rmarkdown
- biomartr

### Setup

First install required package in R:

- pandoc (May differ by distro)

```R
install.packages("rmarkdown")

# To render a website
rmarkdown::render_site()
```

```bash
python map/process.py
python -m http.server 9000 --directory map
```

Then, map with a graph will be locally available at port 9000, visit:
http://localhost:9000/

### Organizational links

* [Doodle poll](https://doodle.com/poll/dmgb7n3n2wdp69hc)
* [Course discussion forum](https://is.muni.cz/auth/discussion/predmetove/fi/jaro2020/PB051/?fakulta=1433;obdobi=7644;predmet=1196217)

## Sources of relevant data

* [NCBI: SARS-CoV-2 (Severe acute respiratory syndrome coronavirus 2) Sequences](https://www.ncbi.nlm.nih.gov/genbank/sars-cov-2-seqs/)
* [University of Oxford: Our World in Data COVID-19 page](https://ourworldindata.org/coronavirus#up-to-date-n-co-v-2019-data-working-group-data)
* [WHO Situation reports](https://www.who.int/emergencies/diseases/novel-coronavirus-2019/situation-reports/)
* [John Hopkins University Data and Links](https://github.com/CSSEGISandData/COVID-19)
* [More than 10,000 UK sequences](https://www.cogconsortium.uk/data/)
* [GISAID](gisaid.org)
* [Italian data](https://github.com/pcm-dpc)
* [Czech Min of Health coronavirus webpage](https://onemocneni-aktualne.mzcr.cz/covid-19)
* [Slovak Public Health authority coronavirus webpage](http://www.uvzsr.sk/index.php?option=com_content&view=category&layout=blog&id=250&Itemid=153)
* [GA4GH coronavirus beacon](https://blog.dnastack.com/dnastack-launches-covid-19-beacon-to-accelerate-sharing-genomic-data-in-the-fight-against-novel-9c6132b5b44b)
* [Canadian open data](https://github.com/ishaberry/Covid19Canada)
* [Iceland data](https://covid.is/data)
* [Daily data from ECDC](https://ecdc.europa.eu/en/publications-data/download-todays-data-geographic-distribution-covid-19-cases-worldwide)
* [UCSC Genome Browser - coronavirus page](http://genome.ucsc.edu/goldenPath/newsarch.html#040320)
* [Nextstrain annotation data](http://data.nextstrain.org/ncov.json)

## Software

* [HMMER 3.0](http://hmmer.org/)
* [flexdashboard](https://rmarkdown.rstudio.com/flexdashboard/)
* [covid-cli](https://www.ostechnix.com/track-coronavirus-disease-2019-covid-19-statistics-from-commandline/)
* [coronavirus R data package](https://github.com/RamiKrispin/coronavirus)

## References

* [Wikipedia background information](https://en.wikipedia.org/wiki/Severe_acute_respiratory_syndrome_coronavirus_2)
* [Theories of SARS-CoV-2 origins](https://www.nature.com/articles/s41591-020-0820-9)
* [Cohen (2020): Mining coronavirus genomes for clues to the outbreak’s origins](https://www.sciencemag.org/news/2020/01/mining-coronavirus-genomes-clues-outbreak-s-origins)
* [Analysis of Belgian cases in R](https://r-bloggers.com/corona-in-belgium/amp)
* [Quick NGS data analysis](https://medium.com/analytics-vidhya/data-analysis-of-coronavirus-and-its-host-basic-bioinformatics-methods-with-code-1eb0b103270)
* [Data integration, manipulation and visualization of phylogenetic trees](https://yulab-smu.github.io/treedata-book/)
* [GIT Handbook](https://guides.github.com/introduction/git-handbook/)
* [RMarkdown information](https://rmarkdown.rstudio.com/lesson-1.html)
* [These guys were faster than us](https://nextstrain.org/ncov) :-)
* [COVID19 country plots from JH data](https://mtholder.github.io/covid19plots/)
* [Extrapolation of country trends](https://covid19-dash.github.io/)
* [Jindra Lacko 's github](https://github.com/jlacko/koronavirus)
* [Log-scaled comparison of countries starting from x days](https://mentalbreaks.shinyapps.io/covid19/)
* [SARS-CoV-2 protein alignments](http://slim.icr.ac.uk/proviz/)
* [Coronaviruses 101 video](https://www.youtube.com/watch?v=8_bOhZd6ieM)
* [Boni et al., 2020. Evolutionary origins of the SARS-CoV-2 sarbecovirus lineage responsible for the COVID-19 pandemic](https://www.biorxiv.org/content/10.1101/2020.03.30.015008v1)
