import copy
import csv
import json

from colorhash import ColorHash

assets = "./map/assets/"
counts = {}
history = {}
days = {}

reader = csv.reader(open(rf"{assets}/sequences.csv"), delimiter=',')
filtered = list(filter(lambda p: p[2].strip() != '', reader))

with open(rf"{assets}/slim-3.json") as file:
    alpha3 = json.load(file)

    """
    Fix some countries
    """
    alpha3.append({'name': "South Korea", 'alpha-3': "KOR"})
    alpha3.append({'name': "Czechia", 'alpha-3': "CZE"})

for s in filtered:
    collected_at = s[5]

    if collected_at not in days:
        days[collected_at] = 0

for s in filtered:
    collected_at = s[5]
    code = s[2].upper().split(":")[0].strip()
    matches = list(filter(lambda t: t['alpha-3'] == code or t['name'].upper() == code or code in t['name'].upper(), alpha3))

    if len(matches) > 0:
        code = matches[0]['alpha-3']
        if code in counts:
            counts[code] += 1
        else:
            counts[code] = 1

        if code not in history:
            history[code] = {
                "label": matches[0]['name'],
                "backgroundColor": ColorHash(matches[0]['name']).hex,
                "data": copy.copy(days)
            }

        history[code]["data"][collected_at] += 1

for key, value in history.items():
    history[key]["data"] = list(value["data"].values())

with open(rf"{assets}/countries.geojson") as file:
    data = json.load(file)
    for feature in data['features']:
        feature['properties']['counts'] = 0
        try:
            feature['properties']['counts'] = counts[feature['properties']['ISO_A3']]
        except KeyError:
            pass

with open(f"{assets}/stats.geojson.json", 'w') as outfile:
    json.dump(data, outfile)


with open(f"{assets}/history.json", 'w') as outfile:
    json.dump({
        "datasets": list(history.values()),
        "labels": list(days.keys())
    }, outfile)
