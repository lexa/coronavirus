# Map and graph visualisation

## Setup

Run data processing by executing:

```bash
pip install colorhash # For graph colors
python map/process.py # in root directory
```